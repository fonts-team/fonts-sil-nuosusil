Source: fonts-sil-nuosusil
Section: fonts
Priority: optional
Maintainer: Debian Fonts Task Force <debian-fonts@lists.debian.org>
Uploaders: Nicolas Spalinger <nicolas.spalinger@sil.org>,
           Daniel Glassey <wdg@debian.org>,
           Hideki Yamane <henrich@debian.org>,
           Bobby de Vos <bobby_devos@sil.org>
Build-Depends: debhelper-compat (= 13)
Standards-Version: 4.6.2
Homepage: https://software.sil.org/nuosu
Vcs-Git: https://salsa.debian.org/fonts-team/fonts-sil-nuosusil.git
Vcs-Browser: https://salsa.debian.org/fonts-team/fonts-sil-nuosusil
Rules-Requires-Root: no

Package: fonts-sil-nuosusil
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Description: Unicode font for Yi (a script used in southwestern China)
 The Nuosu SIL font was originally named SIL Yi and developed in 2000
 as a single Unicode font for the standardized Yi script used by a
 large ethnic group in southwestern China.
 .
 The traditional Yi scripts have been in use for centuries, and have a
 tremendous number of local variants. The script was standardized in the
 1970's by the Chinese government. In the process of standardization, 820
 symbols from the traditional scripts of the Liangshan region were chosen
 to form a syllabary.
 .
 The syllable inventory of a speech variety from Xide County, Sichuan
 was used as the phonological basis for standardization. For the most part
 there is one symbol per phonologically-distinct syllable and vice-versa.
 The direction of writing and reading was standardized as left-to-right.
 Punctuation symbols were borrowed from Chinese, and a diacritic was
 incorporated into the system to mark one of the tones.
 .
 One font from this typeface family is included in this release:
  * Nuosu SIL Regular
 .
 Webfont versions and HTML/CSS examples are also available.
 .
 The full font sources are publicly available at
 https://github.com/silnrsi/font-nuosu
 An open workflow is used for building, testing and releasing.
